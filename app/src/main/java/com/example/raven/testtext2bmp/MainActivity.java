package com.example.raven.testtext2bmp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {
    private Bitmap imgMarker;
    private int width=380; //普通pos打印机纸张宽度
    private int height=380;
    private ImageView iv;
    private Button btnChange;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iv= findViewById(R.id.imgV);
        btnChange= findViewById(R.id.btnchanage);
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgMarker = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                Drawable dw=createDrawable();
                iv.setBackgroundDrawable(dw);
                BitmapDrawable bd = (BitmapDrawable) dw;
                Bitmap bm = bd.getBitmap();
                Log.i("bm size=",bm.getWidth() + " " + bm.getHeight());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private Drawable createDrawable() {
        Bitmap imgTemp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        imgTemp.eraseColor(Color.parseColor("#FFFFFF"));//填充颜色
        Canvas canvas = new Canvas(imgTemp);
        Paint paint = new Paint();
        paint.setDither(true);
        paint.setFilterBitmap(true);
        paint.setAntiAlias(true);//反锯齿
        paint.setStyle(Paint.Style.STROKE);//内空心
        paint.setStrokeWidth(2);  //宽度
        paint.setColor(Color.parseColor("#000000"));
        canvas.drawRect(2, 2, width-2, height-2, paint);//画外框
        canvas.drawLine(2,40,width-2,40,paint); //第一条横线

        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setTextSize(24.0f);
        textPaint.setColor(Color.BLACK);
        Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);//加黑
        textPaint.setTypeface(font);
        canvas.drawText(String.valueOf("我我我:你你你"),10,25,textPaint);
        canvas.drawText(String.valueOf("12345678901"),width-180,25,textPaint);

        TextPaint tp=new TextPaint();
        tp.setTextSize(24.0f);
        tp.setTypeface(font);
        tp.setAntiAlias(true);
        String add="详细：Windows 10 被微软称为最后一代Windows，意指将不断更新，流水不腐户枢不蠹，时刻以最强的姿态为用户服务。";
        int x=0;
        int y=50;
        //使用长文字自动化换行布局
        StaticLayout myStaticLayout1 = new StaticLayout(add, tp, canvas.getWidth()-10, Layout.Alignment.ALIGN_NORMAL, 1f, 0.0f, false);
        canvas.translate(x+10,y);  //文字位置偏移
        myStaticLayout1.draw(canvas);
        canvas.translate(-x-10,-y);//将原点取消偏移
        /**
         * 第二块
         */
        x=2;
        y=height/2;
        canvas.drawLine(x,y,width-x,y,paint); //第二条横线-上下区域分割线
        y+=25;
        canvas.drawText(String.valueOf("他他他:她她她"),10,y,textPaint);
        canvas.drawText(String.valueOf("12345678901"),width-180,y,textPaint);
        y+=15;
        canvas.drawLine(x,y,width-x,y,paint);//第三条横线
        String sAddr="详细：因为这次诺基亚一口气带来了5款新手机，而且有新旗舰、有新定位的功能机、还有跟上潮流的全面屏。";
        StaticLayout myStaticLayout = new StaticLayout(sAddr, tp, canvas.getWidth()-10, Layout.Alignment.ALIGN_NORMAL, 1f, 0.0f, false);
        canvas.translate(10,y+10);
        myStaticLayout.draw(canvas);

        canvas.save(Canvas.ALL_SAVE_FLAG);
        canvas.restore();
        return (Drawable) new BitmapDrawable(getResources(), imgTemp);

    }
}
